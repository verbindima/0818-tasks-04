#include <stdio.h>
#include <math.h>
int main()
{
	int count.st = 0, count.fin = 0;
	unsigned long long des_value = 0, var = 0;
	for (int i = 2; i <1000000;i++)
	{
		var = i;
		while (var != 1)
		{
			if ((var % 2) == 1)
				var = 3 * var+ 1;
			else
				var = var / 2;
			count.st++;
		}
		if (count.st > count.fin)
		{
			count.fin = count.st;
			des_value = i;
		}	
		count.st = 0;
	}
	printf("%I64d = the number that generates the longest sequence.\n", des_value);
	return 0;
}